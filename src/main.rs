use clap::{Clap, IntoApp};
use clap_generate::{generate, generators::{Bash, Fish, PowerShell, Zsh}};

/// This doc string acts as a help message when the user runs '--help'
/// as do all doc strings on fields
#[derive(Clap)]
#[clap(version = "0.1", author = "Tim Schneider <mail@schneidertim.de>")]
struct Opts {
    /// Sets a custom config file. Could have been an Option<T> with no default too
    #[clap(short, long, default_value = "default.conf")]
    config: String,
    /// Some input. Because this isn't an Option<T> it's required to be used
    input: Option<String>,
    /// A level of verbosity, and can be used multiple times
    #[clap(short, long, parse(from_occurrences))]
    verbose: i32,
    #[clap(subcommand)]
    subcmd: SubCommand,
}

#[derive(Clap)]
enum SubCommand {
    Test(Test),
    GenCompletions(GenCompletions)
}

/// A subcommand for controlling testing
#[derive(Clap)]
struct Test {
    /// Print debug info
    #[clap(short)]
    debug: bool
}

/// Generate a completions file for a specified shell 
#[derive(Clap)]
struct GenCompletions{
    #[clap(possible_values(&["bash", "fish", "powershell", "zsh"]))]
    shell: String
}

fn main() {
    let opts: Opts = Opts::parse();

    // Gets a value for config if supplied by user, or defaults to "default.conf"
    println!("Value for config: {}", opts.config);
    if let Some(input) = opts.input {
        println!("Using input file: {}", input);
    }

    // Vary the output based on how many times the user used the "verbose" flag
    // (i.e. 'myprog -v -v -v' or 'myprog -vvv' vs 'myprog -v'
    match opts.verbose {
        0 => println!("No verbose info"),
        1 => println!("Some verbose info"),
        2 => println!("Tons of verbose info"),
        3 | _ => println!("Don't be crazy"),
    }

    // You can handle information about subcommands by requesting their matches by name
    // (as below), requesting just the name used, or both at the same time
    match opts.subcmd {
        SubCommand::Test(t) => {
            if t.debug {
                println!("Printing debug info...");
            } else {
                println!("Printing normally...");
            }
        },

        SubCommand::GenCompletions(options) => {
            let mut app = Opts::into_app();
            let name : String = String::from(app.get_name());
            let write = &mut std::io::stdout();

            match options.shell.as_str() {
                "bash" => generate::<Bash, _>(&mut app, name, write),
                "fish" =>  generate::<Fish, _>(&mut app, name, write),
                "powershell" => generate::<PowerShell, _>(&mut app, name, write),
                "zsh" => generate::<Zsh, _>(&mut app, name, write),
                _ => panic!("Unknown generator"),
            }
        }
    }
    // more program logic goes here...
}
